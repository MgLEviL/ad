/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class JavaApplication2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        
        String texto = "";
        
        System.out.println("Introduce una cadena de texto");
        texto = scan.nextLine();
        getLastChar(texto);
        
        public static char getLastChar(String texto){
            int tamaño = texto.length();
            return texto.charAt(tamaño - 1);
        }
        
    }
    
}
