/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication13;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author user
 */
public class JavaApplication13 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // TODO code application logic here
        //crear jugadores
        Jugador j1 = new Jugador("pepe", "12345678k", 1.75f, 70);
        Jugador j2 = new Jugador("yeah", "12343338k", 1.85f, 80);
        Jugador j3 = new Jugador();
        
        
        FileOutputStream fl = new FileOutputStream(".datos.txt");
        ObjectOutputStream oj = new ObjectOutputStream(fl);
        
        //grabar objeto
        oj.writeObject(j2);
        
        //cerrar
        oj.close();
        fl.close();
        
        
        //leer objeto
        FileInputStream f = new FileInputStream(".datos.txt");
        ObjectInputStream o = new ObjectInputStream(f);
        
        j3 = (Jugador)o.readObject();
        System.out.println(j3.getAlt() + " "+ j3.getPeso() + j3.getNombre());
        
        //cerrar
        o.close();
        f.close();
    }
    
}
