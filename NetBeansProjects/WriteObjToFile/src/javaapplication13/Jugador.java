/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication13;

import java.io.IOException;
import java.io.Serializable;

/**
 *
 * @author user
 */
public class Jugador implements Serializable{
    private String nombre;
    private String dni;
    private float alt;
    private int peso;
    
    public Jugador() throws IOException{
        super();
    }
    
    public Jugador(String nombre, String dni, float alt, int peso) throws IOException{
        this.alt = alt;
        this.dni = dni;
        this.nombre = nombre;
        this.peso = peso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        if(nombre != "" && nombre != null){
            this.nombre = nombre;
        }
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        if(dni.isEmpty() && dni != null){
            this.dni = dni;
        }
    }

    public float getAlt() {
        return alt;
    }

    public void setAlt(float alt) {
        if(alt > 60 && alt < 220){
            this.alt = alt;
        }
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        if(peso > 0 && peso < 150){
            this.peso = peso;
        }
    }
    
    
    
}
