/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hibernatecrud;

import java.util.Iterator;
import java.util.Scanner;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author mglevil
 */
public class HibernateUtil {

    public static SessionFactory buildSessionFactory() {
        try {
            return new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static void addToDB(Session session, Object obj){
        try{
            session.beginTransaction();
            session.save(obj);
            session.getTransaction().commit();
        }catch(Exception e){
            session.getTransaction().rollback();
        }
    }
    public static void updateObj(Session session, int id, String tabla, String dni,
            String nombre, String tlf, int id_other, String bastidor_up,
            String marca_up, String modelo_up){
        
        try{
            session.beginTransaction();
            
            if(tabla.equals("Clientes")){
                Clientes c = (Clientes) session.get(Clientes.class, id);
                c.setDni(dni);
                c.setNombre(nombre);
                c.setTlf(tlf);

                session.update(c);
                
            }else if(tabla.equals("Mecanicos")){
                Mecanicos mec = (Mecanicos) session.get(Mecanicos.class, id);
                mec.setDni(dni);
                mec.setNombre(nombre);
                mec.setTlf(tlf);

                session.update(mec);
                
            }else if(tabla.equals("Proveedores")){
                Proveedores pro = (Proveedores) session.get(Proveedores.class, id);
                pro.setIdproveedor(id_other);
                pro.setNombre(nombre);
                pro.setMarca(marca_up);

                session.update(pro);
            }
            
            else if(tabla.equals("Coches")){
                Coches co = (Coches) session.get(Coches.class, id);
                
                co.setIdcoche(id_other);
                co.setMarca(marca_up);
                co.setModelo(modelo_up);
                co.setNBastidor(bastidor_up);

                session.update(co);               
            }
            
            session.getTransaction().commit();
        }catch(Exception e){
            session.getTransaction().rollback();
        }
    }
    
    public static void showObj(Session session, String tabla){
        Query q = session.createQuery("from " + tabla);

        if(tabla.equals("Clientes")){
            Iterator<Clientes> it = q.iterate();
            Clientes c;
            System.out.println("ID\t DNI\t Nombre\t Tlf");
            while (it.hasNext()) {
                c = it.next();
                System.out.println(c.getIdcliente()+ "\t" + c.getDni() + "\t" + c.getNombre() + "\t" + c.getTlf());
            }
        }else if(tabla.equals("Coches")){
            Iterator<Coches> it = q.iterate();
            Coches c;
            System.out.println("ID\t Bastidor\t Marca\t Modelo");
            while (it.hasNext()) {
                c = it.next();
                System.out.println(c.getIdcoche()+ "\t" + c.getNBastidor()+ "\t" + c.getMarca()+ "\t" + c.getModelo());
            }
        }else if(tabla.equals("Proveedores")){
            Iterator<Proveedores> it = q.iterate();
            Proveedores p;
            System.out.println("ID\t Nombre\t Marca");
            while (it.hasNext()) {
                p = it.next();
                System.out.println(p.getIdproveedor()+ "\t" + p.getNombre()+ "\t" + p.getMarca());
            }
        }else if(tabla.equals("Mecanicos")){
            Iterator<Mecanicos> it = q.iterate();
            Mecanicos m;
            System.out.println("ID\t DNI\t Nombre\t Tlf");
            while (it.hasNext()) {
                m = it.next();
                System.out.println(m.getIdmecanico()+ "\t" + m.getDni() + "\t" + m.getNombre() + "\t" + m.getTlf());
            }
        }
    }
        
    public static void delObj(Session session, Object obj, int id){
        try{
            session.beginTransaction();
            obj = session.load(obj.getClass(), id);
            session.delete(obj);
            session.getTransaction().commit();
        }catch(Exception e){
            session.getTransaction().rollback();
            System.out.println("No se pudo borrar, o bien tiene que eliminar la relacion primero");
        }
            
    }
    
    public static void asignarProveedoresAcoches(Session session){
        try{
            
            Scanner scan = new Scanner(System.in);
            System.out.println("Asignar Proveedor a coche");

            session.beginTransaction();
            System.out.println("-------- Lista Coches -------");
            showObj(session, "Coches");
            System.out.println("-------- Lista Proveedores --------");
            showObj(session, "Proveedores");

            System.out.println("Introducir ID de proveedor:");
            int n = scan.nextInt();

            Proveedores pro = (Proveedores) session.get(Proveedores.class, n);
            System.out.println("Introduce ID del coche");
            n = scan.nextInt();

            Coches co = (Coches) session.get(Coches.class, n);
            pro.setCoches(co);

            session.update(co);
            session.update(pro);
            session.getTransaction().commit();
            
        }catch(Exception ex){
            session.getTransaction().rollback();
            ex.getMessage();
            System.err.println("Uno de los IDs introducidos no existe");
        }
    }
}
