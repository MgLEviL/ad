/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import hibernatecrud.Clientes;
import hibernatecrud.Coches;
import hibernatecrud.HibernateUtil;
import hibernatecrud.Mecanicos;
import hibernatecrud.Proveedores;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mglevil
 */
public class TestCRUD {
    Session session = null;
    
    public TestCRUD() {
        SessionFactory instancia = HibernateUtil.buildSessionFactory();
        session = instancia.openSession();
    }
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testProbarConexionBd() {
        assertTrue("La sesion no se ha iniciado", session.isConnected());     
    }
    @Test
    public void testConstructoresClientes() {
            Clientes cl = null;       
            assertNull("Tiene que ser nulo", cl);
            
            Clientes cl1 = new Clientes("hola", "111111111", "999999999");       
            assertNotNull("No puede ser nulo", cl1);
    }   
    
    @Test
    public void testConstructoresCoches() {
            Coches co = null;       
            assertNull("Tiene que ser nulo", co);
            
            Coches co1 = new Coches(0, "123655555t5ty7y6y", "mnb", "yx");       
            assertNotNull("No puede ser nulo", co1);
    }
        
    @Test
    public void testContructoresProveedores() {
            Proveedores pro = null;       
            assertNull("Tiene que ser nulo", pro);
            
            Proveedores pro1 = new Proveedores(0, "coches fasts", "aets");       
            assertNotNull("No puede ser nulo", pro1);
    }
    
    @Test
    public void testConstructoresMecanicos() {
            Mecanicos mec = null;       
            assertNull("Tiene que ser nulo", mec);
            
            Mecanicos mec1 = new Mecanicos("hola", "111111111", "999999999");       
            assertNotNull("No puede ser nulo", mec1);
    }
    
    @Test
    public void testCrearNuevasFilas() {
        Mecanicos mec1 = new Mecanicos("hola", "111111111", "999999999");  
        Proveedores pro1 = new Proveedores(0, "coches fasts", "aets"); 
        Coches co1 = new Coches(0, "123655555t5ty7y6y", "mnb", "yx");    
        Clientes cl1 = new Clientes("hola", "111111111", "999999999");  
        
        
        session.save(cl1);
        session.save(co1);
        session.save(pro1);
        session.save(mec1);
    }
    
    @Test
    public void testUpdateFilas() {
        Proveedores pro1 = new Proveedores(5, "coches fasts", "aets"); 
        Coches co1 = new Coches(3, "123655555t5ty7y6y", "mnb", "yx");    

        session.save(co1);
        session.save(pro1);       

        pro1 = (Proveedores) session.get(Proveedores.class, 5);
        co1 = (Coches) session.get(Coches.class, 3);
        
        pro1.setIdproveedor(2);
        pro1.setMarca("kawa");
        pro1.setNombre("coches manolo");
        
        co1.setIdcoche(2);
        co1.setMarca("mercedes");
        co1.setModelo("QZY");
        co1.setNBastidor("23423523bgdh2v");

        session.update(co1);
        session.update(pro1);
    }
    
    @Test
    public void testDeleteRows() {
        Proveedores pro1 = new Proveedores(5, "coches fasts", "aets"); 
        Coches co1 = new Coches(3, "123655555t5ty7y6y", "mnb", "yx");    

        session.save(co1);
        session.save(pro1);

        pro1 = (Proveedores) session.get(Proveedores.class, 5);
        co1 = (Coches) session.get(Coches.class, 3);
        
        session.delete(co1);
        session.delete(pro1);
    }
    
}
